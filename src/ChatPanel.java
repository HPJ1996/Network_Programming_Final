import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by hpj_1996 on 2017/5/14.
 */
public class ChatPanel extends JPanel {

    private JTextArea messageArea;

    private SocketChannel sc;
    int chatServerPort;
    String ip;

    public ChatPanel(String ip, int port) {
        super();

        this.ip = ip;
        this.chatServerPort = port;

        this.onCreate();
        this.connectServer();

        Timer timer = new Timer();
        TimerTask receiveMessageTask = new TimerTask() {
            @Override
            public void run() {
                receiveMessage();
            }
        };
        timer.schedule(receiveMessageTask, 3000, 1000);
    }

    public void onCreate() {

        messageArea = new JTextArea();
        messageArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(messageArea);

        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        JTextField textField = new JTextField();
        JButton enterButton = new JButton("Enter");

        ActionListener sendMessageAction = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input = textField.getText();
                textField.setText("");
                // System.out.println(input);

                addMessage( "我：" + input );
                sendMessage(input);
            }
        };
        textField.addActionListener(sendMessageAction);
        enterButton.addActionListener(sendMessageAction);

        JPanel tempPanel = new JPanel(new BorderLayout());
        tempPanel.add(textField, BorderLayout.CENTER);
        tempPanel.add(enterButton, BorderLayout.EAST);

        this.setLayout(new BorderLayout());
        this.add(tempPanel, BorderLayout.SOUTH);
        this.add(scrollPane, BorderLayout.CENTER);

    }

    protected void connectServer() {
        try {

            sc = SocketChannel.open();
            sc.connect(new InetSocketAddress( ip, chatServerPort ));
            addMessage("系統：連線成功！");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    protected void sendMessage(String message) {
        try {
            ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());
            sc.write(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void receiveMessage() {

        try {
            ByteBuffer b = ByteBuffer.allocate(100);
            int len = sc.read(b);
            if(len > 0) {
                String temp = new String(b.array());

                int emptyIndex = temp.indexOf(' ');
                if(emptyIndex > 0) {
                    temp = temp.substring(0, emptyIndex);
                }
                addMessage(temp);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    protected void addMessage(String s) {
        messageArea.append( getTime() + "\n" );
        int stringCutLength = 20;
        while ( s.length() > stringCutLength ) {
            messageArea.append( s.substring(0, stringCutLength) + "\n" );
            s = s.substring(stringCutLength);
        }
        messageArea.append( s + "\n\n" );
    }

    private String getTime() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.MONTH) + "/" + c.get(Calendar.DAY_OF_MONTH) + " " +
                c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND);
    }

}
