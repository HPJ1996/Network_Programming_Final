import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Created by hpj_1996 on 2017/5/21.
 */
public class ChatServerThread implements Runnable {

    Thread thread;
    SocketChannel sc1;
    SocketChannel sc2;
    String message;
    String replyMessage;

    public ChatServerThread(SocketChannel sc1, SocketChannel sc2) {
        this.sc1 = sc1;
        this.sc2 = sc2;

        firstMessage();
        try {
            this.sc1.configureBlocking(false);
            this.sc2.configureBlocking(false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.thread = new Thread(this);
        this.thread.start();
    }

    private void firstMessage() {
        String message = "系統:開始聊天！";
        ByteBuffer buffer1 = ByteBuffer.wrap(message.getBytes());
        ByteBuffer buffer2 = ByteBuffer.wrap(message.getBytes());
        try {
            sc1.write(buffer1);
            sc2.write(buffer2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        while (true) {

            ByteBuffer bb1 = ByteBuffer.allocate(50);
            ByteBuffer bb2 = ByteBuffer.allocate(50);

            try {

                int len1 = sc1.read(bb1);
                if( len1 > 0 ) {
                    message = new String(bb1.array());
                    int emptyIndex = message.indexOf(' ');
                    if(emptyIndex > 0) {
                        message = message.substring(0, emptyIndex);
                    }
                    System.out.println("Receive message : "	+ message);

                    replyMessage = "對手：" + message;
                    ByteBuffer bufferReply = ByteBuffer.wrap(replyMessage.getBytes());
                    sc2.write(bufferReply);
                }

                int len2 = sc2.read(bb2);
                if( len2 > 0 ) {
                    message = new String(bb2.array());
                    int emptyIndex = message.indexOf(' ');
                    if(emptyIndex > 0) {
                        message = message.substring(0, emptyIndex);
                    }
                    System.out.println("Receive message : "	+ message);

                    replyMessage = "對手：" + message;
                    ByteBuffer bufferReply = ByteBuffer.wrap(replyMessage.getBytes());
                    sc1.write(bufferReply);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
