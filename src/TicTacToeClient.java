import javax.swing.*;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.*;

/**
 * Created by hpj_1996 on 2017/5/14.
 */
public class TicTacToeClient extends JFrame {

    protected TicTacToeChatPanel chatPanel;
    protected TicTacToeGamingBoard gamePanel;

    public TicTacToeClient(String ip, int port) {
        super();
        this.chatPanel = new TicTacToeChatPanel(ip, port);
        this.gamePanel = new TicTacToeGamingBoard();

        this.chatPanel.setGamingBoard( this.gamePanel );
        this.gamePanel.setChatPanel( this.chatPanel );

        this.createFrame();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }

    protected void createFrame() {

        int CHAT_PANEL_WIDTH = 300;
        int CHAT_PANEL_HEIGHT = 500;
        chatPanel.setBorder(new EmptyBorder(5,5,5,5));
        chatPanel.setBackground(Color.LIGHT_GRAY);
        chatPanel.setPreferredSize(new Dimension(CHAT_PANEL_WIDTH, CHAT_PANEL_HEIGHT));

        int GAME_PANEL_LENGTH = 500;
        gamePanel.setBackground(Color.BLACK);
        gamePanel.setPreferredSize(new Dimension(GAME_PANEL_LENGTH, GAME_PANEL_LENGTH));

        int WIDTH = 800;
        int HEIGHT = 500;
        this.setTitle("經典對戰遊戲附雙人交談空間佐傳輸控制協定");
        this.setSize(WIDTH, HEIGHT);

        this.setLayout(new BorderLayout());
        this.add(gamePanel, BorderLayout.WEST);
        this.add(chatPanel, BorderLayout.EAST);
    }

    public class TicTacToeChatPanel extends JPanel {

        TicTacToeGamingBoard gb;
        private JTextArea messageArea;

        private SocketChannel sc;
        int chatServerPort;
        String ip;

        public TicTacToeChatPanel(String ip, int port) {
            super();

            this.ip = ip;
            this.chatServerPort = port;

            this.onCreate();
            this.connectServer();

            java.util.Timer timer = new java.util.Timer();
            TimerTask receiveMessageTask = new TimerTask() {
                @Override
                public void run() {
                    receiveMessage();
                }
            };
            timer.schedule(receiveMessageTask, 3000, 1000);
        }

        public void onCreate() {

            messageArea = new JTextArea();
            messageArea.setEditable(false);
            JScrollPane scrollPane = new JScrollPane(messageArea);

            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

            JTextField textField = new JTextField();
            JButton enterButton = new JButton("Enter");

            ActionListener sendMessageAction = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String input = textField.getText();
                    textField.setText("");
                    // System.out.println(input);

                    addMessage( "我：" + input );
                    sendMessage(input);
                }
            };
            textField.addActionListener(sendMessageAction);
            enterButton.addActionListener(sendMessageAction);

            JPanel tempPanel = new JPanel(new BorderLayout());
            tempPanel.add(textField, BorderLayout.CENTER);
            tempPanel.add(enterButton, BorderLayout.EAST);

            this.setLayout(new BorderLayout());
            this.add(tempPanel, BorderLayout.SOUTH);
            this.add(scrollPane, BorderLayout.CENTER);

        }

        protected void connectServer() {
            try {

                sc = SocketChannel.open();
                sc.connect(new InetSocketAddress( ip, chatServerPort ));
                addMessage("系統：連線成功！");

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        protected void sendMessage(String message) {
            try {
                ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());
                sc.write(buffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        protected void receiveMessage() {

            try {
                ByteBuffer b = ByteBuffer.allocate(100);
                int len = sc.read(b);
                if(len > 0) {
                    String temp = new String(b.array());

                    int emptyIndex = temp.indexOf(' ');
                    if(emptyIndex > 0) {
                        temp = temp.substring(0, emptyIndex);
                    }

                    if(temp.equals("系統:開始遊戲！你是Ｏ")) {
                        gb.myTime = true;
                        gb.token = "Ｏ";
                        addMessage(temp);
                    } else if(temp.equals("系統:開始遊戲！你是Ｘ")) {
                        gb.token = "Ｘ";
                        addMessage(temp);
                    } else if(temp.startsWith("System")) {
                        String[] splitString = temp.split("-");
                        gb.buttons[Integer.parseInt(splitString[2])].setText(splitString[1]);
                        gb.myTime = true;
                    } else if(temp.startsWith("Winner")) {
                        addMessage("系統：" + temp);
                        gb.initial();
                    } else if(temp.startsWith("Tie")) {
                        addMessage("系統：雙方平手！");
                        gb.initial();
                    } else {
                        addMessage(temp);
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        protected void addMessage(String s) {
            messageArea.append( getTime() + "\n" );
            int stringCutLength = 20;
            while ( s.length() > stringCutLength ) {
                messageArea.append( s.substring(0, stringCutLength) + "\n" );
                s = s.substring(stringCutLength);
            }
            messageArea.append( s + "\n\n" );
        }

        private String getTime() {
            Calendar c = Calendar.getInstance();
            return c.get(Calendar.MONTH) + "/" + c.get(Calendar.DAY_OF_MONTH) + " " +
                    c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND);
        }

        public void setGamingBoard(TicTacToeGamingBoard gb) {
            this.gb = gb;
        }

    }

    public class TicTacToeGamingBoard extends JPanel implements ActionListener {

        TicTacToeChatPanel cp;
        String token;

        JButton[] buttons;
        boolean myTime;

        public TicTacToeGamingBoard() {

            buttons = new JButton[9];
            this.setLayout(new GridLayout(3,3));

            int count = 0;
            for(int i=0 ; i<buttons.length ; i++) {

                buttons[i] = new JButton();
                buttons[i].addActionListener(this);
                buttons[i].setActionCommand( Integer.toString(count++) );
                buttons[i].setFont(new Font("蘋方-繁", Font.BOLD, 150));
                this.add(buttons[i]);
            }
        }

        public void initial() {
            for(JButton button : buttons) {
                button.setText("");
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            if( myTime ) {

                int buttonNumber = Integer.parseInt( e.getActionCommand() );
                JButton button = (JButton)e.getSource();

                if( button.getText().equals("") ) {
                    button.setText( token );
                    myTime = false;
                    cp.sendMessage( "System-" + token + "-" + buttonNumber );
                } else {
                    cp.addMessage("系統：這裡已經被下了！");
                }
            } else {
                cp.addMessage("系統：現在不是輪到你！");
            }
        }

        public void setChatPanel(TicTacToeChatPanel cp) {
            this.cp = cp;
        }

    }


}
