import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Created by hpj_1996 on 2017/5/22.
 */
public class TicTacToeServerThread implements Runnable {

    Thread thread;
    SocketChannel sc1;
    SocketChannel sc2;
    String message;
    String replyMessage;

    String[] gameStatus = new String[9];

    public TicTacToeServerThread(SocketChannel sc1, SocketChannel sc2) {
        this.sc1 = sc1;
        this.sc2 = sc2;

        firstMessage();
        try {
            this.sc1.configureBlocking(false);
            this.sc2.configureBlocking(false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.thread = new Thread(this);
        this.thread.start();
    }

    private void firstMessage() {
        cleanGameStatus();
        try {
            Thread.sleep(3000);
            ByteBuffer buffer1 = ByteBuffer.wrap("系統:開始遊戲！你是Ｏ".getBytes());
            ByteBuffer buffer2 = ByteBuffer.wrap("系統:開始遊戲！你是Ｘ".getBytes());
            sc1.write(buffer1);
            sc2.write(buffer2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        while (true) {

            ByteBuffer bb1 = ByteBuffer.allocate(50);
            ByteBuffer bb2 = ByteBuffer.allocate(50);

            try {

                int len1 = sc1.read(bb1);
                if( len1 > 0 ) {
                    message = new String(bb1.array());
                    int emptyIndex = message.indexOf(' ');
                    if(emptyIndex > 0) {
                        message = message.substring(0, emptyIndex);
                    }
                    System.out.println("Receive message : "	+ message);

                    if(message.startsWith("System")) {
                        String[] splitString = message.split("-");
                        gameStatus[Integer.parseInt(splitString[2])] = splitString[1];
                        ByteBuffer bufferReply = ByteBuffer.wrap(message.getBytes());
                        sc2.write(bufferReply);

                        Thread.sleep(500);
                        if ( checkWin(splitString[1]) ) {
                            message = "Winner-" + splitString[1];
                            ByteBuffer b = ByteBuffer.wrap(message.getBytes());
                            sc1.write(b);
                            b = ByteBuffer.wrap(message.getBytes());
                            sc2.write(b);
                            cleanGameStatus();
                        } else if( checkTie() ) {
                            message = "Tie";
                            cleanGameStatus();
                        }
                    } else {
                        replyMessage = "對手：" + message;
                        ByteBuffer bufferReply = ByteBuffer.wrap(replyMessage.getBytes());
                        sc2.write(bufferReply);
                    }
                }

                int len2 = sc2.read(bb2);
                if( len2 > 0 ) {
                    message = new String(bb2.array());
                    int emptyIndex = message.indexOf(' ');
                    if(emptyIndex > 0) {
                        message = message.substring(0, emptyIndex);
                    }
                    System.out.println("Receive message : "	+ message);

                    if(message.startsWith("System")) {
                        String[] splitString = message.split("-");
                        gameStatus[Integer.parseInt(splitString[2])] = splitString[1];
                        ByteBuffer bufferReply = ByteBuffer.wrap(message.getBytes());
                        sc1.write(bufferReply);

                        Thread.sleep(500);
                        if ( checkWin(splitString[1]) ) {
                            message = "Winner-" + splitString[1];
                            ByteBuffer b = ByteBuffer.wrap(message.getBytes());
                            sc1.write(b);
                            b = ByteBuffer.wrap(message.getBytes());
                            sc2.write(b);
                            cleanGameStatus();
                        } else if( checkTie() ) {
                            message = "Tie";
                            cleanGameStatus();
                        }
                    } else {
                        replyMessage = "對手：" + message;
                        ByteBuffer bufferReply = ByteBuffer.wrap(replyMessage.getBytes());
                        sc1.write(bufferReply);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean checkWin(String token) {
        if(gameStatus[0].equals(token) && gameStatus[1].equals(token) && gameStatus[2].equals(token)) {
            return true;
        } else if(gameStatus[3].equals(token) && gameStatus[4].equals(token) && gameStatus[5].equals(token)) {
            return true;
        } else if(gameStatus[6].equals(token) && gameStatus[7].equals(token) && gameStatus[8].equals(token)) {
            return true;
        } else if(gameStatus[0].equals(token) && gameStatus[4].equals(token) && gameStatus[8].equals(token)) {
            return true;
        } else if(gameStatus[2].equals(token) && gameStatus[4].equals(token) && gameStatus[6].equals(token)) {
            return true;
        } else if(gameStatus[0].equals(token) && gameStatus[3].equals(token) && gameStatus[6].equals(token)) {
            return true;
        } else if(gameStatus[1].equals(token) && gameStatus[4].equals(token) && gameStatus[7].equals(token)) {
            return true;
        } else if(gameStatus[2].equals(token) && gameStatus[5].equals(token) && gameStatus[8].equals(token)) {
            return true;
        } else {
//            System.out.println("沒人贏！");
            return false;
        }
    }

    public boolean checkTie() {
        for(String s : gameStatus) {
            if(s.equals("")) {
//                System.out.println("沒平手！");
                return false;
            }
        }
        return true;
    }

    public void cleanGameStatus() {
        for (int i=0 ; i<9 ; i++) {
            gameStatus[i] = "";
        }
    }
}
