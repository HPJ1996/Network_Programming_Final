import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by hpj_1996 on 2017/5/14.
 */
public class StartingFrame extends JFrame {

    private static int WIDTH = 500;
    private static int HEIGHT = 300;

    String ip = "127.0.0.1";


    public StartingFrame() {
        super();
        this.createFrame();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }

    protected void createFrame() {

        this.setTitle("經典對戰遊戲附雙人交談空間佐傳輸控制協定");
        this.setSize(WIDTH, HEIGHT);
        JButton button1 = new JButton("套餐Ａ（TicTacToe）");
        JButton button2 = new JButton("套餐Ｂ（PaperScissorsStone）");
        JButton button3 = new JButton("單點（Chat）");
        JButton button4 = new JButton("未來主廚推薦");

        button1.addActionListener(new OpenTicTacToeAction( this ));
        button2.addActionListener(new OpenJustPaperScissorsStoneAction( this ));
        button3.addActionListener(new OpenJustChatAction( this ));
//        button4.addActionListener(new OpenGameAction( this, new AbstractGame() ));

        this.setLayout(new GridLayout(2,2));
        this.add(button1);
        this.add(button2);
        this.add(button3);
        this.add(button4);
    }

    protected class OpenTicTacToeAction implements ActionListener {

        StartingFrame frame;

        @Override
        public void actionPerformed(ActionEvent e) {

            TicTacToeClient game = new TicTacToeClient( ip, 9488 );
            game.setVisible( true );
            frame.setVisible(false);
        }

        OpenTicTacToeAction(StartingFrame frame) {
            this.frame = frame;
        }
    }

    protected class OpenGameAction implements ActionListener {
        AbstractGame gamePanel;
        StartingFrame frame;

        @Override
        public void actionPerformed(ActionEvent e) {

            ChatPanel chatPanel = new ChatPanel( ip, 9487 );

            GamingFrame game = new GamingFrame( chatPanel, gamePanel );
            game.setVisible( true );
            frame.setVisible(false);
        }

        OpenGameAction(StartingFrame frame, AbstractGame gamePanel) {
            this.gamePanel = gamePanel;
            this.frame = frame;
        }
    }

    protected class OpenJustChatAction implements ActionListener {

        StartingFrame frame;

        @Override
        public void actionPerformed(ActionEvent e) {

            ChatPanel chatPanel = new ChatPanel( ip, 9487 );

            JustChatFrame justChatFrame = new JustChatFrame( chatPanel );
            justChatFrame.setVisible( true );
            frame.setVisible(false);
        }

        OpenJustChatAction(StartingFrame frame) {
            this.frame = frame;
        }
    }
    protected class OpenJustPaperScissorsStoneAction implements ActionListener {

        StartingFrame frame;

        @Override
        public void actionPerformed(ActionEvent e) {

            PaperScissorsStoneClient game = new PaperScissorsStoneClient(ip, 9489);
            game.setVisible(true);
            frame.setVisible(false);
        }

        OpenJustPaperScissorsStoneAction(StartingFrame frame) {
            this.frame = frame;
        }
    }
}
