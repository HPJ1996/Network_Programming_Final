import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.*;
/**
 * Created by huangtaiming on 2017/5/23.
 */
public class PaperScissorsStoneClient extends JFrame {
    protected PaperScissorsStoneChatPanel chatPanel;
    protected PaperScissorsStoneGamingBoard gamePanel;

    public PaperScissorsStoneClient(String ip, int port) {
        super();
        this.chatPanel = new PaperScissorsStoneChatPanel(ip, port);
        this.gamePanel = new PaperScissorsStoneGamingBoard();

        this.chatPanel.setGamingBoard( this.gamePanel );
        this.gamePanel.setChatPanel( this.chatPanel );

        this.createFrame();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }

    protected void createFrame() {

        int CHAT_PANEL_WIDTH = 300;
        int CHAT_PANEL_HEIGHT = 380;
        chatPanel.setBorder(new EmptyBorder(5,5,5,5));
        chatPanel.setBackground(Color.LIGHT_GRAY);
        chatPanel.setPreferredSize(new Dimension(CHAT_PANEL_WIDTH, CHAT_PANEL_HEIGHT));

        int GAME_PANEL_LENGTH = 500;
        gamePanel.setBackground(Color.BLACK);
        gamePanel.setPreferredSize(new Dimension(GAME_PANEL_LENGTH, GAME_PANEL_LENGTH));

        int WIDTH = 800;
        int HEIGHT = 380;
        this.setTitle("經典對戰遊戲附雙人交談空間佐傳輸控制協定");
        this.setSize(WIDTH, HEIGHT);

        this.setLayout(new BorderLayout());
        this.add(gamePanel, BorderLayout.WEST);
        this.add(chatPanel, BorderLayout.EAST);
    }

    public class PaperScissorsStoneChatPanel extends JPanel {

        PaperScissorsStoneGamingBoard gb;
        private JTextArea messageArea;

        private SocketChannel sc;
        int chatServerPort;
        String ip;

        public PaperScissorsStoneChatPanel(String ip, int port) {
            super();

            this.ip = ip;
            this.chatServerPort = port;

            this.onCreate();
            this.connectServer();

            java.util.Timer timer = new java.util.Timer();
            TimerTask receiveMessageTask = new TimerTask() {
                @Override
                public void run() {
                    receiveMessage();
                }
            };
            timer.schedule(receiveMessageTask, 3000, 1000);
        }

        public void onCreate() {

            messageArea = new JTextArea();
            messageArea.setEditable(false);
            JScrollPane scrollPane = new JScrollPane(messageArea);

            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

            JTextField textField = new JTextField();
            JButton enterButton = new JButton("Enter");

            ActionListener sendMessageAction = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String input = textField.getText();
                    textField.setText("");
                    // System.out.println(input);

                    addMessage( "我：" + input );
                    sendMessage(input);
                }
            };
            textField.addActionListener(sendMessageAction);
            enterButton.addActionListener(sendMessageAction);

            JPanel tempPanel = new JPanel(new BorderLayout());
            tempPanel.add(textField, BorderLayout.CENTER);
            tempPanel.add(enterButton, BorderLayout.EAST);

            this.setLayout(new BorderLayout());
            this.add(tempPanel, BorderLayout.SOUTH);
            this.add(scrollPane, BorderLayout.CENTER);

        }

        protected void connectServer() {
            try {

                sc = SocketChannel.open();
                sc.connect(new InetSocketAddress( ip, chatServerPort ));
                addMessage("系統：連線成功！");

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        protected void sendMessage(String message) {
            try {
                ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());
                sc.write(buffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        protected void receiveMessage() {
            Image img;
            try {
                ByteBuffer b = ByteBuffer.allocate(100);
                int len = sc.read(b);
                if(len > 0) {
                    String temp = new String(b.array());

                    int emptyIndex = temp.indexOf(' ');
                    if(emptyIndex > 0) {
                        temp = temp.substring(0, emptyIndex);
                    }
                    System.out.println("Yee : " + temp);
                    // temp => 對手：你出了 Paper
                    if(temp.equals("對手出了 Paper")) {
                        img = ImageIO.read(getClass().getResource("resources/paper.png"));
                        gb.enemyFinger.setIcon(new ImageIcon(img)); //換對手的拳！！！！！
                    }else if(temp.equals("對手出了 Scissors")) {
                        img = ImageIO.read(getClass().getResource("resources/scissors.png"));
                        gb.enemyFinger.setIcon(new ImageIcon(img)); //換對手的拳！！！！！
                    }else if(temp.equals("對手出了 Stone")) {
                        img = ImageIO.read(getClass().getResource("resources/stone.png"));
                        gb.enemyFinger.setIcon(new ImageIcon(img)); //換對手的拳！！！！！
                    }

                    if(temp.equals("系統:開始遊戲！")) {
                        gb.myTime = true;
                        addMessage(temp);
                    }

                    else {
                        addMessage(temp);
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        protected void addMessage(String s) {
            messageArea.append( getTime() + "\n" );
            int stringCutLength = 20;
            while ( s.length() > stringCutLength ) {
                messageArea.append( s.substring(0, stringCutLength) + "\n" );
                s = s.substring(stringCutLength);
            }
            messageArea.append( s + "\n\n" );
        }

        private String getTime() {
            Calendar c = Calendar.getInstance();
            return c.get(Calendar.MONTH) + "/" + c.get(Calendar.DAY_OF_MONTH) + " " +
                    c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND);
        }

        public void setGamingBoard(PaperScissorsStoneGamingBoard gb) {
            this.gb = gb;
        }

    }

    public class PaperScissorsStoneGamingBoard extends JPanel implements ActionListener {

        PaperScissorsStoneChatPanel cp;
        String token;

        JLabel enemyFinger;

        boolean myTime;

        public PaperScissorsStoneGamingBoard() {
            Image img;

            this.setLayout(new BorderLayout());
            JButton paperButton = new JButton();
            paperButton.addActionListener(this);
            paperButton.setActionCommand("Paper");
            try {
                img = ImageIO.read(getClass().getResource("resources/paper.png"));
                paperButton.setIcon(new ImageIcon(img)); // 加一下圖案歐！！！！
            } catch (Exception e) {

            }

            JButton scissorsButton = new JButton();
            scissorsButton.addActionListener(this);
            scissorsButton.setActionCommand("Scissors");
            try {
                img = ImageIO.read(getClass().getResource("resources/scissors.png"));
                scissorsButton.setIcon(new ImageIcon(img)); // 加一下圖案歐！！！！
            } catch (Exception e) {

            }

            JButton stoneButton = new JButton();
            stoneButton.addActionListener(this);
            stoneButton.setActionCommand("Stone");
            try {
                img = ImageIO.read(getClass().getResource("resources/stone.png"));
                stoneButton.setIcon(new ImageIcon(img)); // 加一下圖案歐！！！！
            } catch (Exception e) {

            }

            JPanel buttonPanel = new JPanel();
            buttonPanel.setPreferredSize(new Dimension(500, 180));
            buttonPanel.setLayout(new GridLayout(1,3));
            buttonPanel.add(paperButton);
            buttonPanel.add(scissorsButton);
            buttonPanel.add(stoneButton);


            JPanel enemyFingerLayoutPanel = new JPanel(new GridLayout(1,3));
            enemyFinger = new JLabel(); // 這個顯示對手出什麼拳用的
            enemyFinger.setBorder(new EmptyBorder(5,95,5,95));
            enemyFinger.setBackground(Color.GREEN);
            enemyFingerLayoutPanel.add( new JPanel());
            enemyFingerLayoutPanel.add(enemyFinger);
            enemyFingerLayoutPanel.add( new JPanel());

//            enemyFinger.setIcon(); // 換對手的拳歐！！！

            this.add(buttonPanel, BorderLayout.SOUTH);
            this.add(enemyFingerLayoutPanel, BorderLayout.CENTER);

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String choose = e.getActionCommand();

            // choose 會是 Paper 或 Scissors 或 Stone
            System.out.println(choose);
            cp.sendMessage( "你出了 " + choose );

        }

        public void setChatPanel(PaperScissorsStoneChatPanel cp) {
            this.cp = cp;
        }


    }

}
