import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Created by huangtaiming on 2017/5/23.
 */
public class PaperScissorsStoneThread implements Runnable {

    Thread thread;
    SocketChannel sc1;
    SocketChannel sc2;
    String message;
    String replyMessage;
    String sc1Ch;
    String sc2Ch;
    public PaperScissorsStoneThread(SocketChannel sc1, SocketChannel sc2) {
        this.sc1 = sc1;
        this.sc2 = sc2;

        firstMessage();
        try {
            this.sc1.configureBlocking(false);
            this.sc2.configureBlocking(false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.thread = new Thread(this);
        this.thread.start();
    }

    private void firstMessage() {
        cleanGameStatus();
        try {
            Thread.sleep(3000);
            ByteBuffer buffer1 = ByteBuffer.wrap("系統:開始遊戲！".getBytes());
            ByteBuffer buffer2 = ByteBuffer.wrap("系統:開始遊戲！".getBytes());
            sc1.write(buffer1);
            sc2.write(buffer2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        String win = "You are win!!";
        String lose = "You are lose!!";
        String tie = "Tie";
        int check;
        int emptyIndex;
        int ok1 = 0;
        int ok2 = 0;
        while (true) {

            ByteBuffer bb1 = ByteBuffer.allocate(50);
            ByteBuffer bb2 = ByteBuffer.allocate(50);

            try {
                if(ok1 != 0 && ok2 != 0) {
                    ok1 = 0;
                    ok2 = 0;
                    check = checkWin();
                    if (check == 1) {
                        message = win;
                        ByteBuffer b = ByteBuffer.wrap(message.getBytes());
                        sc1.write(b);
                        message = lose;
                        b = ByteBuffer.wrap(message.getBytes());
                        sc2.write(b);
                    } else if (check == -1) {
                        message = lose;
                        ByteBuffer b = ByteBuffer.wrap(message.getBytes());
                        sc1.write(b);
                        message = win;
                        b = ByteBuffer.wrap(message.getBytes());
                        sc2.write(b);
                    } else if (check == 0) {
                        message = tie;
                        ByteBuffer b = ByteBuffer.wrap(message.getBytes());
                        sc1.write(b);
                        b = ByteBuffer.wrap(message.getBytes());
                        sc2.write(b);
                    }
//                    check = 2;
//                    cleanGameStatus();
                }else {

                    int len1 = sc1.read(bb1);
                    if (len1 > 0) {
                        message = new String(bb1.array());
                        emptyIndex = message.indexOf(' ');
                        if (emptyIndex > 0) {
                            message = message.substring(0, emptyIndex);
                        }
                        System.out.println("Receive message : " + message);


//                        String[] splitString = message.split("-");
//                        ByteBuffer bufferReply = ByteBuffer.wrap(message.getBytes());
//                        sc2.write(bufferReply);
//                        if(ok1 == 0 || ok2 == 0) {
                            Thread.sleep(500);
//                    if (ok1 == 1 && ok2 == 2) {
                            String[] splitString = message.split(" ");
                            sc1Ch = splitString[1];
                            replyMessage = "對手出了 " + splitString[1];
                            ByteBuffer bufferReply = ByteBuffer.wrap(replyMessage.getBytes());
                            sc2.write(bufferReply);
                            ok1 = 1;
//                        }
                    }

                    int len2 = sc2.read(bb2);
                    if (len2 > 0) {
                        message = new String(bb2.array());
                        emptyIndex = message.indexOf(' ');
                        if (emptyIndex > 0) {
                            message = message.substring(0, emptyIndex);
                        }
                        System.out.println("Receive message : " + message);
//                        String[] splitString = message.split("-");
//                        ByteBuffer bufferReply = ByteBuffer.wrap(message.getBytes());
//                        sc1.write(bufferReply);
                            Thread.sleep(500);
                            String[] splitString = message.split(" ");
                            sc2Ch = splitString[1];
                            replyMessage = "對手出了 " + splitString[1];
                            ByteBuffer bufferReply = ByteBuffer.wrap(replyMessage.getBytes());
                            sc1.write(bufferReply);
                            ok2 = 1;
                    }
                }



            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public int checkWin() {
        if(sc1Ch.equals("Paper") && sc2Ch.equals("Scissors")) {
            return -1;
        }else if(sc1Ch.equals("Scissors") && sc2Ch.equals("Paper")){
            return 1;
        }else if(sc1Ch.equals("Scissors") && sc2Ch.equals("Stone")) {
            return -1;
        }else if(sc1Ch.equals("Stone") && sc2Ch.equals("Scissors")) {
            return 1;
        }else if(sc1Ch.equals("Stone") && sc2.equals("Paper")) {
            return -1;
        }else if(sc1Ch.equals("Paper") && sc2.equals("Stone")) {
            return 1;
        }else if(sc1Ch.equals("Paper") && sc2Ch.equals("Paper")) {
            return 0;
        }else if(sc1Ch.equals("Scissors") && sc2Ch.equals("Scissors")) {
            return 0;
        }else if(sc1Ch.equals("Stone") && sc2Ch.equals("Stone")) {
            return 0;
        }else {
            return 2;
        }
    }

    public void cleanGameStatus() {

    }
}
