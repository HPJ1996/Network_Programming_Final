import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by hpj_1996 on 2017/5/22.
 */
public class JustChatFrame extends JFrame {

    private ChatPanel chatPanel;

    public JustChatFrame(ChatPanel chatPanel) {
        super();
        this.chatPanel = chatPanel;
        this.createFrame();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }

    protected void createFrame() {

        int WIDTH = 300;
        int HEIGHT = 500;

        chatPanel.setBorder(new EmptyBorder(5,5,5,5));
        chatPanel.setBackground(Color.LIGHT_GRAY);
        chatPanel.setPreferredSize(new Dimension(WIDTH, HEIGHT));

        this.setTitle("雙人交談空間佐傳輸控制協定");
        this.setSize(WIDTH, HEIGHT);

        this.setLayout(new BorderLayout());
        this.add(chatPanel, BorderLayout.EAST);
    }
}
