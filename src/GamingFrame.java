import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by hpj_1996 on 2017/5/14.
 */
public class GamingFrame extends JFrame {

    protected ChatPanel chatPanel;
    protected AbstractGame gamePanel;

    public GamingFrame(ChatPanel chatPanel, AbstractGame absGame) {
        super();
        this.chatPanel = chatPanel;
        this.gamePanel = absGame;
        this.createFrame();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }

    protected void createFrame() {

        int CHAT_PANEL_WIDTH = 300;
        int CHAT_PANEL_HEIGHT = 500;
        chatPanel.setBorder(new EmptyBorder(5,5,5,5));
        chatPanel.setBackground(Color.LIGHT_GRAY);
        chatPanel.setPreferredSize(new Dimension(CHAT_PANEL_WIDTH, CHAT_PANEL_HEIGHT));

        int GAME_PANEL_LENGTH = 500;
        gamePanel.setBackground(Color.BLUE);
        gamePanel.setPreferredSize(new Dimension(GAME_PANEL_LENGTH, GAME_PANEL_LENGTH));


        int WIDTH = 800;
        int HEIGHT = 500;
        this.setTitle("經典對戰遊戲附雙人交談空間佐傳輸控制協定");
        this.setSize(WIDTH, HEIGHT);

        this.setLayout(new BorderLayout());
        this.add(gamePanel, BorderLayout.WEST);
        this.add(chatPanel, BorderLayout.EAST);
    }

}
