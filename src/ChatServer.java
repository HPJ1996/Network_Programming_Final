import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/**
 * Created by hpj_1996 on 2017/5/21.
 */
public class ChatServer {

    public static void main(String args[]) {
        ServerSocketChannel ssc;
        ServerSocket serverSocket;

        int port = 9487;
        int count = 0;

        SocketChannel sc1;
        SocketChannel sc2;

        try {
            ssc = ServerSocketChannel.open();
            serverSocket = ssc.socket();
            serverSocket.bind(new InetSocketAddress(port));

            System.out.println("聊天伺服器開始運作");

            while (true) {
                sc1 = ssc.accept();
                System.out.println( ++count + "A使用者連線");
                String message = "系統：請稍等另一位玩家連線";
                ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());
                sc1.write(buffer);

                sc2 = ssc.accept();
                System.out.println( count + "B使用者連線");
                new ChatServerThread(sc1, sc2);
            }

        } catch(IOException e) {
            e.printStackTrace();
        }

    }
}
